**[Ce site](https://ctassart.gitlab.io/demositeweb/) explique comment créer un site web statique avec R et le déployer sur gitlab (mais aussi github)**    
Il sert de support à une présentation aux utilisateurs et utilisatrices de la **communauté R de l'Insee hauts-de-France** et vise à être une ressource utile à quiconque souhaiterait obtenir quelques conseils pour créer un site web (avec `Rmd`) et le déployer, écrire en markdown, utiliser du CSS...

Le site contient plusieurs "pages" :
- `Accueil` :
- `Étapes` : : résume le processus de création et de déploiement, en pas à pas
- `Intro` : Présentation des objectifs et du plan de la séance
- `Gitlab` : Les différentes étapes dans `gitlab` (création repo et déploiement)
- `R Studio` : Les différentes étapes dans `RStudio` (création projet, fichiers à créer, git...)
- `Insérer code R` : les `chunks` et la réalisation de divers outputs
- `Tuto Markdown` : les bases du "langage"
- `Tuto CSS` : les bases du `CSS`
- `Ressources` : les sites de référence pour élaborer ce supprot.

Merci tout particulièrement au [tuto de Lisa DeBruine](https://debruine.github.io/tutorials/webpages.html)
